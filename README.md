## Objective

The objective of this ETL pipeline based project is to design a Java-based system that can efficiently extract, process, and transform data without utilizing any third-party libraries.

## Description

Create a series of 3 programs that work together in a pipeline

**Code A: Extraction Engine Implementation:**
Write a well-formed program using Java to extract data from NewsAPI.
(Do not use any online program codes or scripts, which is not part of the official API documentation and specification.)
Search keywords (These are Not case sensitive)– “Canada”, “University”, “Dalhousie”, “Halifax”, “Canada Education”, “Moncton”, “hockey”, “Fredericton”, “celebration”

**Code B: Data-Processing Engine Implementation:**
This program is initiated with the Extraction Engine and waits for any incoming data. Once the raw data is captured using the API, this program writes the news contents, and the titles to files. Note: This is an automated process, there is no manual file creation or writing process involved. In addition, each file should contain only 5 news articles or less.

**Code C: Transformation Engine Implementation:**
This program should automatically clean and transform the data stored in the files, and then upload each record to new MongodB database myMongoNews. You can create a single collection or multiple collections depending on your design. For cleaning and transformation: Remove special characters, URLs, emoticons etc. Write your own regular expression logic. You cannot use libraries such as, jsoup, JTidy



## Deliverables: Pseudocode of data extraction engine

![Psuedocode_DataExtractionEngine](./deliverables/Psuedocode_DataExtractionEngine.png)

### Deliverables: Flowchart of the transformation engine

![Flowchart_TransformationEngine](./deliverables/Flowchart_TransformationEngine.png)

