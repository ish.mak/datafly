import Engines.DriverEngine;

/**
 This is the main class of the News Data Pipeline system.
 */
public class Main {
    /**
     This is the main method which creates an instance of {@link Engines.DriverEngine} and calls the {@code newsDataPipeline} method to initiate the pipeline process.
     @param args command line arguments
     */
    public static void main(String[] args){
        DriverEngine driverEngine = new DriverEngine();
        driverEngine.newsDataPipeline();
    }
}