/**
 The Logger class provides static methods to log various steps in the news data extraction, processing, and transformation
 process. It includes methods to log the initialization of each step, API response data, and processed article data.
 */
package Utils;
import EngineAssist.NewsData.ArticleData;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Logger {
    /**
     * Logs the initialization of the news data extraction process.
     */
    public static void initNewsExtraction(){
        System.out.println("\n-------------------------------------");
        System.out.println("News :: Data Extraction :: Initiated!");
        System.out.println("-------------------------------------\n");
    }

    /**
     * Logs the initialization of the news data processing process.
     */
    public static void initNewsDataProcessing(){
        System.out.println("\n-------------------------------------");
        System.out.println("News :: Data Processing :: Initiated!");
        System.out.println("-------------------------------------\n");
    }

    /**
     * Logs the initialization of the news data transformation process.
     */
    public static void initNewsDataTransformer(){
        System.out.println("\n-------------------------------------");
        System.out.println("News :: Data Transforming :: Initiated!");
        System.out.println("-------------------------------------\n");
    }

    /**
     * Logs the API response data from the news data extraction process.
     * @param futures a list of CompletableFutures representing the API response data for each keyword.
     * @param keywords a list of keywords for which the API response data was fetched.
     */
    public static void newsApiResponse(List<CompletableFuture<ArticleResponse>> futures, List<String> keywords) {
        System.out.println("\n-------------------------------------");
        System.out.println("\nNews :: Data Extraction :: fetch\n");

        for (int i = 0; i < futures.size(); i++) {
            try {
                ArticleResponse response = futures.get(i).get();
                System.out.printf("Results for keyword '%s': %d articles%n", keywords.get(i), response.getArticles().size());
            } catch (InterruptedException | ExecutionException e) {
                System.err.printf("Error while fetching data for keyword '%s': %s%n", keywords.get(i), e.getMessage());
            }
        }

        System.out.println("-------------------------------------\n");
    }

    /**
     * Logs the processed article data from the news data processing process.
     * @param articleDataList a list of ArticleData objects representing the processed article data.
     */
    public static void articleData(List<ArticleData> articleDataList){
        System.out.println("\n-------------------------------------");
        for (ArticleData articleData : articleDataList) {
            System.out.println();
            System.out.println("Title: " + articleData.getTitle());
            System.out.println("Content: " + articleData.getContent());
            System.out.println();
        }
        System.out.println("-------------------------------------\n");
    }

    public static void dataExtractionSuccess(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Success: Data Extraction...");
        System.out.println("---------------------------------------------------\n");
    }

    public static void dataExtractionFailure(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Failure: Data Extraction...");
        System.out.println("---------------------------------------------------\n");
    }

    public static void dataProcessingSuccess(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Success: Data Processing...");
        System.out.println("---------------------------------------------------\n");
    }

    public static void dataProcessingFailure(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Failure: Data Processing...");
        System.out.println("---------------------------------------------------\n");
    }

    public static void dataTransformationSuccess(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Success: Data Transformation...");
        System.out.println("---------------------------------------------------\n");
    }

    public static void dataTransformationFailure(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Failure: Data Transformation...");
        System.out.println("---------------------------------------------------\n");
    }

    public static void newsDataPipelineSuccess(){
        System.out.println("\n---------------------------------------------------");
        System.out.println("Pipeline Success :: Extract -> Process -> Transform");
        System.out.println("---------------------------------------------------\n");
    }
}