/**
 The DriverEngine class is responsible for orchestrating the news data pipeline
 */
package Engines;

public class DriverEngine {
    public void newsDataPipeline(){
        // initiate data extractor and processor
        Engines.DataExtractor.NewsData.News dataExtractor = new Engines.DataExtractor.NewsData.News();
        Engines.DataProcessor.NewsData.News dataProcessor = new Engines.DataProcessor.NewsData.News();

        // fetch data (this fetch will call extractor, and extractor will call transformer)
        dataExtractor.fetch();
    }
}