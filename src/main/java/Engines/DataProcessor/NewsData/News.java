/**
 * This class represents a News object that processes ArticleResponses and stores them in files.
 */
package Engines.DataProcessor.NewsData;

import EngineAssist.NewsData.ArticleData;
import Utils.Logger;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class News {
    private String newsFolderPath = "src/main/java/Storage/News";
    private String titleFolderPath = "src/main/java/Storage/News/Title";
    private String contentFolderPath = "src/main/java/Storage/News/Content";

    /**
     * Sets up folders for news storage.
     */
    private void setupFolders() {
        createOrClearFolder(newsFolderPath);
        createOrClearFolder(titleFolderPath);
        createOrClearFolder(contentFolderPath);
    }

    /**
     * Creates or clears the specified folder.
     *
     * @param folderPath the path of the folder to create or clear
     */
    private void createOrClearFolder(String folderPath) {
        File folder = new File(folderPath);
        if (!folder.exists()) {
            folder.mkdir();
        } else {
            clearFolder(folder);
        }
    }

    /**
     * Clears a given folder by deleting all its files and subfolders.
     *
     * @param folder the folder to clear
     */
    private void clearFolder(File folder) {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                clearFolder(file);
            } else {
                file.delete();
            }
        }
    }

    /**
     * Processes a list of ArticleResponses and returns a list of ArticleData objects.
     *
     * @param articleResponseList the list of ArticleResponses to process
     * @return a list of ArticleData objects
     */
    private List<ArticleData> fetchArticleDataList(List<CompletableFuture<ArticleResponse>> articleResponseList) {
        List<ArticleData> articleDataList = new ArrayList<>();

        for (int i = 0; i < articleResponseList.size(); i++) {
            try {
                ArticleResponse articleResponse = articleResponseList.get(i).get();

                articleResponse.getArticles().forEach(article -> {
                    String title = article.getTitle();
                    String content = article.getContent();
                    articleDataList.add(new ArticleData(title, content));
                });
            } catch (InterruptedException | ExecutionException e) {
                System.err.printf("Error while fetching data for keyword");
            }
        }

        return articleDataList;
    }

    /**
     * Populates the article data in separate files.
     *
     * @param articleDataList the list of ArticleData objects to populate
     * @throws IOException if there's an error while writing to file
     */
    private void populateDataInFile(List<ArticleData> articleDataList) throws IOException {
        int numFiles = (int) Math.ceil((double) articleDataList.size() / 5);

        for (int i = 0; i < numFiles; i++) {
            int startIndex = i * 5;
            int endIndex = Math.min((i + 1) * 5, articleDataList.size());

            File outputTitleFile = new File(titleFolderPath + "/" + (i + 1) + ".txt");
            File outputContentFile = new File(contentFolderPath + "/" + (i + 1) + ".txt");

            BufferedWriter titleWriter = new BufferedWriter(new FileWriter(outputTitleFile));
            BufferedWriter contentWriter = new BufferedWriter(new FileWriter(outputContentFile));

            // Write the elements to the file
            for (int j = startIndex; j < endIndex; j++) {

                String title = articleDataList.get(j).getTitle() == null ? "" : articleDataList.get(j).getTitle();
                titleWriter.write(title);
                titleWriter.newLine();

                String content = articleDataList.get(j).getContent() == null ? "" : articleDataList.get(j).getContent();
                contentWriter.write(content);
                contentWriter.newLine();
            }

            titleWriter.close();
            contentWriter.close();
        }
    }

    /**
     * Write the processed news data to files.
     *
     * @param articleResponseList A list of CompletableFuture objects that contain news article data.
     * @return True if writing the data to files was successful, false otherwise.
     */
    public boolean writeDataToFile(List<CompletableFuture<ArticleResponse>> articleResponseList) {

        // get only required data
        List<ArticleData> articleDataList = fetchArticleDataList(articleResponseList);

        // populate data in files
        try {
            populateDataInFile(articleDataList);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * This method processes the news article data.
     *
     * @param articleResponseList A list of CompletableFuture objects that contain news article data.
     */
    public void process(List<CompletableFuture<ArticleResponse>> articleResponseList) {
        // setup folders
        setupFolders();

        Boolean isSuccess = writeDataToFile(articleResponseList);

        if (isSuccess) {
            Logger.dataProcessingSuccess();
            callNewsDataTransformer();
        }else {
            Logger.dataProcessingFailure();
        }
    }

    /**
     * This method calls the NewsDataTransformer to transform and store the processed news article data.
     */
    public void callNewsDataTransformer() {
        Engines.DataTransformer.NewsData.News dataTransformer = new Engines.DataTransformer.NewsData.News();
        dataTransformer.transformAndStore();
    }
}