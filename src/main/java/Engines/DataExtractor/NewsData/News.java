/**
 * The News class provides methods for extracting news data using the NewsAPI library.
 * It fetches top headlines for a predefined set of keywords and returns a list of CompletableFutures
 * that contain the response data from the API.
 */
package Engines.DataExtractor.NewsData;

import Utils.Logger;
import com.kwabenaberko.newsapilib.NewsApiClient;
import com.kwabenaberko.newsapilib.models.request.TopHeadlinesRequest;
import com.kwabenaberko.newsapilib.models.response.ArticleResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static Constants.Constants.NEWS_API_KEY;

public class News {

    /**
     * Constructs a new TopHeadlinesRequest object with the specified keyword.
     *
     * @param keyword the keyword to search for in the news headlines
     * @return the TopHeadlinesRequest object
     */
    private TopHeadlinesRequest createTopHeadlinesRequest(String keyword) {
        return new TopHeadlinesRequest.Builder()
                .q(keyword)
                .build();
    }

    /**
     * Constructs a new ArticlesResponseCallback object with the specified keyword and future.
     *
     * @param keyword the keyword to search for in the news headlines
     * @param future  the CompletableFuture object to complete with the response data from the API
     * @return the ArticlesResponseCallback object
     */
    private NewsApiClient.ArticlesResponseCallback createCallback(String keyword, CompletableFuture<ArticleResponse> future) {
        return new NewsApiClient.ArticlesResponseCallback() {
            @Override
            public void onSuccess(ArticleResponse response) {
                future.complete(response);
            }

            @Override
            public void onFailure(Throwable throwable) {
                future.completeExceptionally(throwable);
            }
        };
    }

    /**
     * Fetches top headlines for the specified keyword using the NewsAPI library.
     *
     * @param keyword the keyword to search for in the news headlines
     * @param apiKey  the API key to use for the NewsAPI library
     * @return a CompletableFuture object that will contain the response data from the API
     */
    private CompletableFuture<ArticleResponse> fetchTopHeadlinesForKeyword(String keyword, String apiKey) {
        CompletableFuture<ArticleResponse> future = new CompletableFuture<>();
        NewsApiClient client = new NewsApiClient(apiKey);

        client.getTopHeadlines(
                createTopHeadlinesRequest(keyword),
                createCallback(keyword, future));
        return future;
    }

    /**
     * This method fetches top headlines for a predefined set of keywords using the NewsAPI library.
     */
    public void fetch() {
        List<String> keywords = Arrays.asList("Canada", "University", "Dalhousie", "Halifax", "Canada Education", "Moncton", "hockey", "Fredericton", "celebration");
        List<CompletableFuture<ArticleResponse>> futures = new ArrayList<>();

        for (String keyword : keywords) {
            CompletableFuture<ArticleResponse> future = fetchTopHeadlinesForKeyword(keyword, NEWS_API_KEY);
            futures.add(future);
        }

        boolean isSuccess = !futures.isEmpty();
        if (isSuccess) {
            Logger.dataExtractionSuccess();
            callNewsDataProcessor(futures);
        } else {
            Logger.dataExtractionFailure();
        }
    }

    /**
     * This method calls the NewsDataProcessor to process the list of ArticleResponse objects.
     * @param articleResponseList The list of ArticleResponse objects to be processed by the NewsDataProcessor.
     */
    public void callNewsDataProcessor(List<CompletableFuture<ArticleResponse>> articleResponseList) {
        Engines.DataProcessor.NewsData.News dataProcessor = new Engines.DataProcessor.NewsData.News();
        dataProcessor.process(articleResponseList);
    }
}