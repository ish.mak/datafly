
/**
 * This class handles the transformation and storage of news data from text files. It provides methods for retrieving
 * ArticleData objects from text files, transforming them by removing URLs, emoticons, special characters and whitespace,
 * and storing the transformed data in a MongoDB collection.
 */
package Engines.DataTransformer.NewsData;

import EngineAssist.NewsData.ArticleData;
import Utils.Logger;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.bson.Document;

import static Constants.Constants.MONGODB_CONNECTION_STRING;

public class News {
    // File path of the title and content directories
    private String titleFolderPath = "src/main/java/Storage/News/Title/";
    private String contentFolderPath = "src/main/java/Storage/News/Content/";

    /**
     * Retrieves news data from text files stored in the specified directories.
     *
     * @return a list of ArticleData objects representing the news data retrieved from the text files
     */
    private List<ArticleData> getNewsDataFromFilesStorage() {

        // Get File objects for the title and content directories
        File titleFolder = new File(titleFolderPath);
        File contentFolder = new File(contentFolderPath);

        // Get arrays of File objects representing the individual title and content files
        File[] titleFiles = titleFolder.listFiles();
        File[] contentFiles = contentFolder.listFiles();

        // Create an ArrayList to hold the ArticleData objects
        List<ArticleData> articleDataList = new ArrayList<>();

        // Iterate over the title and content files in parallel, creating ArticleData objects from the corresponding lines
        for (int i = 0; i < titleFiles.length && i < contentFiles.length; i++) {
            String titleFilePath = titleFiles[i].getPath();
            String contentFilePath = contentFiles[i].getPath();

            try (BufferedReader titleReader = new BufferedReader(new FileReader(titleFilePath));
                 BufferedReader contentReader = new BufferedReader(new FileReader(contentFilePath))) {

                // Read the first lines from the title and content files
                String title = titleReader.readLine();
                String content = contentReader.readLine();

                // Continue reading lines until the end of the title file is reached, adding ArticleData objects to
                // articleDataList for each pair of corresponding lines in the title and content files
                while (title != null) {
                    articleDataList.add(new ArticleData(title, content));
                    title = titleReader.readLine();
                    content = contentReader.readLine();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return articleDataList;
    }

    /**
     * Transforms a list of ArticleData by cleaning the content of each article.
     * The method removes URLs, emoticons, special characters, and trims whitespace from the content.
     *
     * @param articleDataList the list of ArticleData to transform
     * @return the cleaned list of ArticleData
     */
    private List<ArticleData> transform(List<ArticleData> articleDataList) {
        List<ArticleData> cleanedDataList = new ArrayList<>();
        for (ArticleData articleData : articleDataList) {
            String title = articleData.getTitle();
            String content = articleData.getContent();

            if (!Objects.equals(content, "")) {
                // Remove URLs
                content = content.replaceAll("(https?|ftp)://[\\w_-]+(\\.[\\w_-]+)+([\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-])?", "");

                // Remove emoticons
                content = content.replaceAll(":[\\w]+:", "");

                content = content.replaceAll("\\[\\+\\d+ chars\\]", "");

                // Remove special characters
                content = content.replaceAll("[^\\w\\s]+", "");

                // Trim whitespace
                content = content.trim();
            }

            cleanedDataList.add(new ArticleData(title, content));
        }
        return cleanedDataList;
    }

    /**
     * Stores a list of article data into a MongoDB collection named "MyMongoNews"
     *
     * @param articleDataList a list of ArticleData objects containing the title and content of articles
     * @return true if the articles were successfully stored, false otherwise
     */
    private boolean store(List<ArticleData> articleDataList) {
        try (MongoClient mongoClient = MongoClients.create(MONGODB_CONNECTION_STRING)) {
            MongoDatabase database = mongoClient.getDatabase("dmwa");
            database.createCollection("MyMongoNews");
            MongoCollection<Document> collection = database.getCollection("MyMongoNews");

            List<Document> documents = new ArrayList<>();
            for (ArticleData articleData : articleDataList) {
                Document document = new Document();
                document.append("title", articleData.getTitle());
                document.append("content", articleData.getContent());
                documents.add(document);
            }

            collection.insertMany(documents);
        } catch (MongoException e) {
            System.out.println(e);
            return false;
        }

        return true;
    }

    /**
     * This method transforms and stores the news data by performing the following steps:
     * Fetches news data from file storage.
     * Transforms the fetched data by removing URLs, emoticons, special characters, and trimming whitespaces.
     * Stores the transformed data to MongoDB collection "MyMongoNews".
     */
    public void transformAndStore() {
        // Fetch news data from file storage
        List<ArticleData> articleDataList = getNewsDataFromFilesStorage();
        // Transform the fetched data
        List<ArticleData> transformedArticleDataList = transform(articleDataList);
        // Store the transformed data to MongoDB collection "MyMongoNews"
        Boolean isSuccess =  store(transformedArticleDataList);

        if(isSuccess){
            Logger.dataTransformationSuccess();
        }else{
            Logger.dataTransformationFailure();
        }
    }
}