/**
 This class contains constant values used in the application.
 */
package Constants;

public class Constants {
    /**
     * The News API key to be used for making requests to the News API.
     */
    public static final String NEWS_API_KEY = ""; // add key

    /**
     * The MongoDB connection string to be used for connecting to the MongoDB database.
     */
    public static final String MONGODB_CONNECTION_STRING = ""; // add connection string
}