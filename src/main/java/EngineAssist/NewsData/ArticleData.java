/**
 * A class that represents the data of a news article, including its title and content.
 */
package EngineAssist.NewsData;

public class ArticleData {
    private String title;
    private String content;

    /**
     * Constructs a new ArticleData object with the given title and content.
     *
     * @param title the title of the article
     * @param content the content of the article
     */
    public ArticleData(String title, String content) {
        this.title = title;
        this.content = content;
    }

    /**
     * Returns the title of the article.
     *
     * @return the title of the article
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the content of the article.
     *
     * @return the content of the article
     */
    public String getContent() {
        return content;
    }

}